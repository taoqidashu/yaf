<?php
require_once __DIR__ . '/init.php';

$application = new \Yaf\Application(APPLICATION_PATH . "/conf/application.ini");
$application->bootstrap()->run();
