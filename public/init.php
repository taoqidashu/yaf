<?php
/* 定义这个常量是为了在application.ini中引用*/
define('APPLICATION_PATH', dirname(__DIR__));
ini_set("display_errors", true);
error_reporting(E_ALL|E_ERROR);
// 设置时区
date_default_timezone_set('Asia/Shanghai');