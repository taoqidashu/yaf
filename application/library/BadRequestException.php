<?php
use Yaf\Exception;

/**
 * Class BadRequestException
 * @desc 业务异常类
 */
class BadRequestException extends Exception
{
}