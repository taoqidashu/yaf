<?php

use Yaf\Application;
use Yaf\Bootstrap_Abstract;
use Yaf\Dispatcher;
use Yaf\Registry;
use Yaf\Loader;

/**
 * Class Bootstrap
 * @name Bootstrap
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:\Yaf\Dispatcher $dispatcher
 * 调用的次序, 和声明的次序相同
 */
class Bootstrap extends Bootstrap_Abstract
{
    /**
     * 初始化配置
     */
    public function _initConfig()
    {
        //把配置保存起来
        $arrConfig = Application::app()->getConfig();
        Registry::set('config', $arrConfig);
        Registry::set('routes', include APPLICATION_PATH. '/conf/route.php');
    }

    /**
     * Autoload 自动载入
     * @param Dispatcher $dispatcher
     */
    public function _initAutoload(Dispatcher $dispatcher)
    {
        // 注册composer
        require_once APPLICATION_PATH.'/vendor/autoload.php';

        // 助手函数
        require_once __DIR__ . '/helpers.php';

        /**
         * 注册其他命名空间     https://www.laruence.com/manual/yaf.autoloader.html
         * 类的加载方法       https://www.laruence.com/manual/yaf.autoloader.rule.html
         */
        // 注册本地类
        //$loader = Loader::getInstance();
        // 申明, 凡是以Foo和Local开头的类, 都是本地类
        //$loader->registerLocalNamespace(array("Foo", "Local"));
    }

    /**
     * 注册插件
     * @param Dispatcher $dispatcher
     */
    public function _initPlugin(Dispatcher $dispatcher)
    {
        $objSamplePlugin = new SamplePlugin();
        $dispatcher->registerPlugin($objSamplePlugin);
    }

    /**
     * 注册路由协议(默认使用简单路由)
     * - 路由注册的顺序很重要, 最后注册的路由协议, 最先尝试路由, 这就有个陷阱. 请注意.
     * @param Dispatcher $dispatcher
     */
    public function _initRoute(Dispatcher $dispatcher)
    {
        /**
         * Yaf_Route_Static默认的路由协议, 分析请求中的request_uri，以/分隔得到Module,Controller,Action
         * Yaf_Route_Simple是基于请求中的query string来做路由（3个参数分别代表在query string中Module, Controller, Action的变量名）
         * Yaf_Route_Supervar是在query string中获取路由信息, 它获取的是一个类似包含整个路由信息的request_uri
         * Yaf_Route_Map映射路由协议，它将REQUEST_URI中以'/'分割的节, 组合在一起, 形成一个分层的控制器或者动作的路由结果
         * Yaf_Route_Rewrite是一个强大的路由协议, 它能满足我们绝大部分的路由需求
         * Yaf_Route_Regex正则路由
         * https://www.laruence.com/manual/yaf.routes.static.html
         * https://www.laruence.com/manual/yaf.routes.usage.html
         *
         * 使用路由器装载路由协议
         */
        $dispatcher->getRouter()->addConfig(Registry::get('routes'));
    }

    /**
     * 注册视图
     * @param Dispatcher $dispatcher
     */
    public function _initView(Dispatcher $dispatcher)
    {
        //在这里注册自己的view控制器，例如smarty,firekylin
        /**
         * 关闭渲染     $dispatcher->disableView();
         * 打开渲染     $dispatcher->enableView();
         */
    }
}
